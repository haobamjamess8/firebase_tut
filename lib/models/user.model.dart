// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

User userFromJson(String str) => User.fromJson(json.decode(str));

String userToJson(User data) => json.encode(data.toJson());

class User {
    User({
        required this.add,
        required this.name,
    });

    String add;
    String name;

    factory User.fromJson(Map<String, dynamic> json) => User(
        add: json["add"],
        name: json["name"],
    );

    Map<String, dynamic> toJson() => {
        "add": add,
        "name": name,
    };
}
