

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fcli/firebase_storage.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:logger/logger.dart';
import './models/user.model.dart';
import 'firebase_options.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  // FireStoreStorage fireStoreStorage = FireStoreStorage();
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    // fireStoreStorage.initialLize();
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
      builder: EasyLoading.init(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final int _counter = 0;
  var f = FireStoreStorage.firestore;
Logger logger = Logger();


//  get(){
// var f = FireStoreStorage.firestore;
// f.collection("user").add({
//   "name":"James",
//   "add":"fdfsd",
// });
// }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: StreamBuilder(
        stream: f.collection("user").snapshots(),
        builder: (context,
           snapshot) {
          if (snapshot.hasData) {
            List<User> data = [];
           snapshot.data!.docs.map((DocumentSnapshot e) {
            
          var d = e.data();
          if (d!= null) {
            var e = d as Map<String,dynamic>;
            User user = User.fromJson(e);
            data.add(user);
            
          }
           }).toList();

         logger.d(data);

       
       
  
      

            
         return ListView.builder(
                itemCount: data.length,
                itemBuilder: (context, index) => Card(
                  color: Colors.grey,
                  child: ListTile(
                    onTap: () {
                      // EasyLoading.showToast(dataDetailsList[index].toString());
                      logger.d(userToJson(data[index]));

                    },
                    title: Text(data[index].name),
                    subtitle: Text(data[index].add),
                  ),
                ),
              );
          }
          return Text(snapshot.connectionState.toString());
        },
      ),
    );
  }
}
